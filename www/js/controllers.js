var theTimes = angular.module('starter', ['ionic', 'ui.router', 'ngAnimate']);
theTimes.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('list', {
      url: '/',
      templateUrl: 'list.html',
      controller: 'ListCtrl',
      cache: false
    })
    .state('view', {
      url: '/view',
      templateUrl: 'view.html',
      controller: 'ViewCtrl'
    });

  $urlRouterProvider.otherwise('/');

});
//==============================================================================
theTimes.service('items', function() {

  this.theQues = function(int) {

    return numbers[int];
  };
  this.theTable = function() {

    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    shuffleArray(numbers);
    return numbers;
  };
  this.theAns = function(int) {

    a1 = int * tryTimes;
    var a2 = a1 + tryTimes;
    var a3 = a1 - tryTimes;
    answers = [a1, a2, a3];
    shuffleArray(answers);

    return answers;
  };
});

theTimes.factory('passedVars', function() {

  passedVars = {};
  passedVars.whichTable = '';
  return passedVars;
});
//==============================================================================
theTimes.controller('ListCtrl', function($scope, passedVars) {
  $scope.input = passedVars;

  $scope.timestable = function(passedVal) {
    tryTimes = passedVal;
  };
  if (window.localStorage.getItem("twos") !== undefined && window.localStorage.getItem("twos") !== null) {
    $scope.tt2 = window.localStorage.getItem("twos") + '/10';
  } else $scope.tt2 = '0/10';
  if (window.localStorage.getItem("threes") !== undefined && window.localStorage.getItem("threes") !== null) {
    $scope.tt3 = window.localStorage.getItem("threes") + '/10';
  } else $scope.tt3 = '0/10';
  if (window.localStorage.getItem("fours") !== undefined && window.localStorage.getItem("fours") !== null) {
    $scope.tt4 = window.localStorage.getItem("fours") + '/10';
  } else $scope.tt4 = '0/10';
  if (window.localStorage.getItem("fives") !== undefined && window.localStorage.getItem("fives") !== null) {
    $scope.tt5 = window.localStorage.getItem("fives") + '/10';
  } else $scope.tt5 = '0/10';
  if (window.localStorage.getItem("sixs") !== undefined && window.localStorage.getItem("sixs") !== null) {
    $scope.tt6 = window.localStorage.getItem("sixs") + '/10';
  } else $scope.tt6 = '0/10';
  if (window.localStorage.getItem("sevens") !== undefined && window.localStorage.getItem("sevens") !== null) {
    $scope.tt7 = window.localStorage.getItem("sevens") + '/10';
  } else $scope.tt7 = '0/10';
  if (window.localStorage.getItem("eights") !== undefined && window.localStorage.getItem("eights") !== null) {
    $scope.tt8 = window.localStorage.getItem("eights") + '/10';
  } else $scope.tt8 = '0/10';
  if (window.localStorage.getItem("nines") !== undefined && window.localStorage.getItem("nines") !== null) {
    $scope.tt9 = window.localStorage.getItem("nines") + '/10';
  } else $scope.tt9 = '0/10';
  if (window.localStorage.getItem("tens") !== undefined && window.localStorage.getItem("tens") !== null) {
    $scope.tt10 = window.localStorage.getItem("tens") + '/10';
  } else $scope.tt10 = '0/10';
  if (window.localStorage.getItem("elevens") !== undefined && window.localStorage.getItem("elevens") !== null) {
    $scope.tt11 = window.localStorage.getItem("elevens") + '/10';
  } else $scope.tt11 = '0/10';
  if (window.localStorage.getItem("twelves") !== undefined && window.localStorage.getItem("twelves") !== null) {
    $scope.tt12 = window.localStorage.getItem("twelves") + '/10';
  } else $scope.tt12 = '0/10';
});
//==============================================================================
theTimes.run(function($ionicPlatform) {

  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
//==============================================================================
theTimes.controller('ViewCtrl', function($scope, items, passedVars, $timeout) {

  $scope.input = passedVars;
  $scope.whichTimes = tryTimes;

  //Mix the tables.
  items.theTable();
  //Set the score.
  $scope.counter = 0;
  $scope.correct = 0;
  $scope.score = $scope.correct + ' / ' + $scope.counter;
  //Get question.
  var q = items.theQues(0);
  //Get the answers.

  var answers = items.theAns(q);
  //Send back.
  $scope.a1 = answers[0];
  $scope.a2 = answers[1];
  $scope.a3 = answers[2];

  $scope.times = q + ' x ' + tryTimes;

  switch (tryTimes) {
    case 2:
      {
        $scope.two = 2;
      }
      break;
    case 3:
      {
        $scope.three = 3;
      }
      break;
    case 4:
      {
        $scope.four = 4;
      }
      break;
    case 5:
      {
        $scope.five = 5;
      }
      break;
    case 6:
      {
        $scope.six = 6;
      }
      break;
    case 7:
      {
        $scope.seven = 7;
      }
      break;
    case 8:
      {
        $scope.eight = 8;
      }
      break;
    case 9:
      {
        $scope.nine = 9;
      }
      break;
    case 10:
      {
        $scope.ten = 10;
      }
      break;
    case 11:
      {
        $scope.eleven = 11;
      }
      break;
    case 12:
      {
        $scope.twelve = 12;
      }
      break;
  }
  $scope.multiplier = q;

  //raise the balloons.
  $scope.balloonClass1 = 'ng-balloonUp1';
  $scope.balloonClass2 = 'ng-balloonUp2';
  $scope.balloonClass3 = 'ng-balloonUp3';

  //----------------------------------------------------------------------------
  //Check answer.
  $scope.give_answer = function(passedVal) {

    //Pop the ballons.
    $scope.balloonClass1 = 'reset-balloon';
    $scope.balloonClass2 = 'reset-balloon';
    $scope.balloonClass3 = 'reset-balloon';

    //Start Balloons on next cycle timeout 0.
    var theTimer = $timeout(function() {
      var theRandom = Math.floor((Math.random() * 3) + 1);
      $scope.balloonClass1 = 'ng-balloonUp' + theRandom;
      theRandom = Math.floor((Math.random() * 3) + 1);
      $scope.balloonClass2 = 'ng-balloonUp' + theRandom;
      theRandom = Math.floor((Math.random() * 3) + 1);
      $scope.balloonClass3 = 'ng-balloonUp' + theRandom;
    }, 0);

    $scope.counter += 1;

    if ($scope.counter < 11) {
      switch (tryTimes) {
        case 2:
          {
            if (($scope.multiplier * $scope.two) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 3:
          {
            if (($scope.multiplier * $scope.three) == passedVal) {
              $scope.correct += 1;
            }
          }
        case 4:
          {
            if (($scope.multiplier * $scope.four) == passedVal) {
              $scope.correct += 1;
            }
          }
        case 5:
          {
            if (($scope.multiplier * $scope.five) == passedVal) {
              $scope.correct += 1;
            }
          }
        case 6:
          {
            if (($scope.multiplier * $scope.six) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 7:
          {
            if (($scope.multiplier * $scope.seven) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 8:
          {
            if (($scope.multiplier * $scope.eight) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 9:
          {
            if (($scope.multiplier * $scope.nine) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 10:
          {
            if (($scope.multiplier * $scope.ten) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 11:
          {
            if (($scope.multiplier * $scope.eleven) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;
        case 12:
          {
            if (($scope.multiplier * $scope.twelve) == passedVal) {
              $scope.correct += 1;
            }
          }
          break;

      }

      if ($scope.counter < 10) {

        q = items.theQues($scope.counter);
        //Get the answers.
        answers = items.theAns(q);
        //Send back.
        $scope.a1 = answers[0];
        $scope.a2 = answers[1];
        $scope.a3 = answers[2];

        $scope.times = q + ' x ' + tryTimes;

        $scope.multiplier = q;


      } else {
        //hide balloons.
        $timeout.cancel(theTimer);
        $scope.balloonClass1 = 'reset-balloon';
        $scope.balloonClass2 = 'reset-balloon';
        $scope.balloonClass3 = 'reset-balloon';

        if ($scope.correct == 10) {
          $scope.times = 'Top score!';
        } else if ($scope.correct > 5) {
          $scope.times = 'Well done!';
        } else {
          $scope.times = 'Good try!';
        }
        $scope.answers_css = 'display:none';
        //save the score.
        switch (tryTimes) {
          case 2:
            {
              window.localStorage.setItem("twos", $scope.correct);
            }
            break;
          case 3:
            {
              window.localStorage.setItem("threes", $scope.correct);
            }
            break;
          case 4:
            {
              window.localStorage.setItem("fours", $scope.correct);
            }
            break;
          case 5:
            {
              window.localStorage.setItem("fives", $scope.correct);
            }
            break;
          case 6:
            {
              window.localStorage.setItem("sixs", $scope.correct);
            }
            break;
          case 7:
            {
              window.localStorage.setItem("sevens", $scope.correct);
            }
            break;
          case 8:
            {
              window.localStorage.setItem("eights", $scope.correct);
            }
            break;
          case 9:
            {
              window.localStorage.setItem("nines", $scope.correct);
            }
            break;
          case 10:
            {
              window.localStorage.setItem("tens", $scope.correct);
            }
            break;
          case 11:
            {
              window.localStorage.setItem("elevens", $scope.correct);
            }
            break;
          case 12:
            {
              window.localStorage.setItem("twelves", $scope.correct);
            }
            break;

        }
      }
      $scope.score = $scope.correct + ' / ' + $scope.counter;
      $scope.add_class = 'fadesback';
    };
  }
});
//==============================================================================
// -> Fisher–Yates shuffle algorithm
var shuffleArray = function(array) {
    var m = array.length,
      t, i;

    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }
  //==============================================================================
